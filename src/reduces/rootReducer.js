const initState = {
    todos: [
        {id:1, content:'Yay, we are here', dueDate:"12-01-2020", dateCompleted: "3-01-2020", complete: false},
        {id:2, content:'Hellp world!', dueDate:"03-04-2020", dateCompleted: "14-01-2020", complete: true},
        {id:3, content:'Oops, nothing!', dueDate:"02-01-2020", dateCompleted: "3-01-2020", complete: false}    
    ],
    deleteItem: [],
    createTodos: [],
    completed: [],
    editTodos: [],
    page: 'Home',
    count: 3,
    redirect: true,
    navigate: false
}

const DELETE_TODO = 'DELETE_TODO';
const CREATE_TODO = 'CREATE_TODO';
const CHECK_TODO = 'CHECK_TODO';
const UPDATE_RESTORE = 'UPDATE_RESTORE';
const EDIT_TODO = 'EDIT_TODO';
const UPDATE_EDIT = 'UPDATE_EDIT';
const NAVI_TODO = 'NAVI_TODO';
const REDIRECT_TRUE = 'REDIRECT_TRUE';
const REDIRECT_FALSE = 'REDIRECT_FALSE';

export const updateRestore = (id) => {
    return ({
        type: UPDATE_RESTORE,
        id: id
    })
}
export const checkTodo = (id) => {

    return ({
        type: CHECK_TODO,
        id
    })
}
export const deleteTodo = (id) => {
    return ({
        type: DELETE_TODO,
        id
    })
}
export const createTodo = (data) => {
    return ({
        type: CREATE_TODO,
        data
    })
}
export const editTodo = (id) => {
    return {
        type: EDIT_TODO,
        id
    }
}
export const updateEditTodo = (data) => {
    return {
        type: UPDATE_EDIT,
        data
    }
}
export const navigateTodo = () => {
    return {
        type : NAVI_TODO
    }
}
export const redirectTrue = () => {
    return {
        type : REDIRECT_TRUE
    }
}
export const redirectFalse = () => {
    return {
        type : REDIRECT_FALSE
    }
}

const rootReducer = (state = initState, action) => {
    switch (action.type) {
        case DELETE_TODO: 
          return Object.assign({}, state, {
                ...state,
                deleteItem: [...state.deleteItem, state.todos.filter((todo) => {
                    return todo.id === action.id
                })[0]].sort((a,b) => a.id - b.id),
                todos: state.todos.filter((todo) => {
                    return todo.id !== action.id
                }),
                
            })
        case CREATE_TODO: 
            return Object.assign({}, state, {
                ...state,
                todos: [...state.todos, action.data],
                createTodos: [...state.createTodos, action.data],
                count: action.data.count,
            })
        case CHECK_TODO: 
            return Object.assign({}, state, {
                todos: state.todos.map((todo) => {
                    const clone ={...todo}
                    todo.id === action.id ? clone.complete = !clone.complete : clone.complete;
                    return clone;
                })
            })
        case UPDATE_RESTORE:
            return Object.assign({}, state, {
                ...state,
                todos: [...state.todos, state.deleteItem.filter((todo) => {
                    return todo.id === action.id
                })[0]].sort((a,b) => a.id - b.id),
                deleteItem: state.deleteItem.filter(item => {
                    return item.id !== action.id
                })
            })
        case EDIT_TODO:
            return Object.assign({}, state, {
                editTodos: state.todos.filter(todo => {
                    return todo.id === action.id
                }),
                todos: state.todos.filter(todo => {
                    return todo.id !== action.id
                }),
                page: 'Edit',
                navigate: false
            })
        case UPDATE_EDIT:
            return Object.assign({}, state, {
                todos: [...state.todos, action.data].sort((a,b) => a.id - b.id),
                editTodos: state.editTodos.filter(todo => {
                        return todo.id === action.id
                    }),
                page: 'Home'
            })
        case NAVI_TODO:
            return Object.assign({}, state, {
                navigate: true
            })
        case REDIRECT_TRUE:
            return Object.assign({}, state, {
                redirect: true
            })
        case REDIRECT_FALSE:
            return Object.assign({}, state, {
                redirect: false
            })
        default:
            return state;
    }
}

export default rootReducer