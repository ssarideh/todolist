import styled from 'styled-components';


export const Label = styled.label`
        span {
            &:before{
                left: 10px !important;
            }
            &:hover {
                &:before { 
                    top: -4px;
                    left: 10px;
                    width: 12px;
                    height: 22px;
                    border-top: 2px solid transparent;
                    border-left: 2px solid transparent;
                    border-right: 2px solid #26a69a;
                    border-bottom: 2px solid #26a69a;
                    transform: rotate(40deg);
                    box-shadow: 2px 7px 20px -4px rgba(0,0,0,0.75);
                    transition: 0.3s ease-in-out;
                } 
            }
        }
    `
  
export const LabelCompleted = styled.label`
    span {
        &:before{
            left: 10px !important;
        }
        &:hover {
            &:before { 
                box-shadow: 2px 7px 20px -4px rgba(0,0,0,0.75);
                transition: 0.3s ease-in-out;
                top: 0 !important;
                width: 18px !important;
                height: 18px !important;
                border: 2px solid #5a5a5a !important;
                transform: rotate(0) !important;
            } 
        }
    `
    
        