import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { deleteTodo, checkTodo, editTodo, navigateTodo } from '../reduces/rootReducer';
import { Label } from '../elements/style';
import { Redirect } from 'react-router';
import { useHistory } from "react-router-dom";


const Home = () => {
    
    const todos = useSelector(state => state.todos);
    const page = useSelector(state => state.page); 
    const redirect = useSelector(state => state.redirect);
    const dispatch = useDispatch();
    const history = useHistory();

    if (!redirect) {
        alert('You must confirm button first!');
        history.push("/Create") 
    }

    const handleChecked = (id) => {
        dispatch(checkTodo(id));
    }
    const handleDelete = (id) => {
        dispatch(deleteTodo(id)); //connect to store for calling the function
    }
    const handleEdit = (id) => {
        dispatch(editTodo(id));
        gotocreate();
    }
    const handleClick = () => {
        dispatch(navigateTodo())
        gotocreate();
    }
    const gotocreate = () => {
        history.push("/Create") 
    }
    const item = todos
        .filter(item => {
            return item.complete !== true
        })
        .map(item => { 
            return (
                <tr key={item.id}>
                    <td>
                        <Label>
                            <input 
                                type="checkbox" 
                                onChange={(e) => handleChecked(item.id)} 
                                checked={item.complete} 
                            />
                            <span></span>
                        </Label>
                        </td>
                        <td>
                            <span>{item.content} </span>
                        </td>
                        <td>
                            <span>{item.dueDate} </span>
                        </td>
                        <td>
                            <span>-</span>
                        </td>
                        <td>
                            <button 
                                onClick={() => handleDelete(item.id)}
                                className="btn-small red darken-3"
                            >X</button>
                        </td>
                        <td>
                            <button 
                                onClick={() => handleEdit(item.id)}
                                className="btn-small blue lighten-2"
                            >Edit</button>
                        </td>
                    </tr>
                )
            })

    const textTitle =  <tr className="text-title"><td colSpan="6">sorry, there's nothing here</td></tr>;
       
    return (
        <div className="container">
            <table className="responsive-table">
                <thead>
                    <tr>
                        <th>
                            <div>Status</div>
                            <div>(Pending)</div>
                        </th>
                        <th>Content</th>
                        <th>Due Date</th>
                        <th>Date Completed</th>
                        <th>Deleting</th>
                        <th>Editing</th>
                    </tr>
                </thead>
                <tbody>
                {item.length === 0 ? textTitle : item}
                </tbody>
            </table>
            <div className="clear"></div>
            <button className="btn-create btn-small red darken-3" onClick={() => handleClick()}>Create</button>
        </div>
    )
}

export default Home