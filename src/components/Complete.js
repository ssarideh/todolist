import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { checkTodo } from '../reduces/rootReducer';
import { LabelCompleted } from '../elements/style';
import { useHistory } from "react-router-dom";
import { Redirect } from 'react-router';


const Complete = () => {
    const todos = useSelector(state => state.todos);
    const redirect = useSelector(state => state.redirect);
    const history = useHistory();
    const dispatch = useDispatch();
    
    if (!redirect) {
        alert('You must confirm button first!');
        history.push("/Create");
    }

    const handleChecked = (id) => {
        dispatch(checkTodo(id))
    }

    const item = todos
        .filter((item) => item.complete === true)
        .map(item => {
                return (
                    <tr key={item.id}>
                        <td>
                            <LabelCompleted>
                                <input 
                                    type="checkbox" 
                                    onChange={(e) => handleChecked(item.id)} 
                                    checked={item.complete} 
                                />
                                <span></span>
                            </LabelCompleted>
                            </td>
                            <td>
                                <span>{item.content} </span>
                            </td>
                            <td>
                                <span>{item.dueDate} </span>
                            </td>
                            <td>
                                <span>{item.dateCompleted} </span>
                            </td>
                        </tr>
                    
                    ) 
        })
    
    const textTitle =  <tr className="text-title"><td colSpan="4">sorry, there's nothing here</td></tr>;

    return (
        <div className="container">
            <table className="responsive-table">
                <thead>
                    <tr>
                        <th>
                            <div>Status</div>
                            <div>(Completed)</div>
                        </th>
                        <th>Content</th>
                        <th>Due Date</th>
                        <th>Date Completed</th>
                    </tr>
                </thead>
                <tbody>
                    {item.length === 0 ? textTitle : item}
                </tbody>
            </table>
        </div>
        
    )
}

export default Complete